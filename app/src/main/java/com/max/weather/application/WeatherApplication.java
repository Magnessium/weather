package com.max.weather.application;

import android.app.Application;
import android.content.Context;


public class WeatherApplication extends Application {


    static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getAppContext() {
        return context;
    }

}
