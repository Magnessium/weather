package com.max.weather.event;

import android.location.Location;

public class OnEvent {
    private int position;
    private Location location;

    public OnEvent(int position, Location location) {
        super();
        this.position = position;
        this.location = location;
    }

    public int getPosition() {
        return position;
    }

    public Location getLocation() {
        return location;
    }
}
