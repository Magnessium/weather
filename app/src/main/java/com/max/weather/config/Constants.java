package com.max.weather.config;

public class Constants {

    public static final String APP = "TAG_";

    public static final String WEATHER_BASE_POINT = "http://api.openweathermap.org/";
    public static final String WEATHER_BASE_POINT_ICON = "http://openweathermap.org/img/w/";
    public static final String EXT_ICON = ".png";

    public static final String FLICKR_BASE_POINT = "https://api.flickr.com/services/";
    public static final String FLICKR_API_SEARCH = "flickr.photos.search";
    public static final String FLICKR_API_KEY = "9b8882adacb33eacc7e832ff93bb3d54";
    public static final int FLICKR_API_ACCURACY = 11;
    public static final String FLICKR_API_FORMAT = "json";
    public static final String FLICKR_API_NOJSON = "nojsoncallback";
    public static final int FLICKR_API_CONTENT_TYPE = 7;
    public static final int FLICKR_API_PAGE = 1;
    public static final int FLICKR_API_PER_PAGE = 1;
    public static final int FLICKR_API_PRIVACY_FILTER = 1;

    public static final String WEATHER_APP_ID = "d093bb73c6af60656663769828589680";
    public static final String WEATHER_UNITS = "metric";

    public static final String SUN_TIME_FORMAT = "hh:mm:ss";

    public static final int WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS = 600;
    public static final long WEATHER_SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MB

    public static final int PLACE_PICKER_REQUEST = 1;
    public static final int PERMISSIONS_REQUEST = 2;
}
