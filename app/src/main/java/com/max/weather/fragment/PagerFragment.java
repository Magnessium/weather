package com.max.weather.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.max.weather.R;
import com.max.weather.adapter.PagerAdapter;
import com.max.weather.misk.RealmHelper;
import com.max.weather.model.DataPage;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static com.max.weather.config.Constants.APP;
import static com.max.weather.config.Constants.PLACE_PICKER_REQUEST;


public class PagerFragment extends Fragment {

    @Bind(R.id.pager) ViewPager pager;
    @Bind(R.id.pager_title_strip) PagerTabStrip pager_title_strip;

    private static final String KEY_POSITION = "position";
    private int position;


    public static PagerFragment newInstance(int position) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(KEY_POSITION, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        setPager(position);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                startPlacePicker();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getContext(), data);
                RealmHelper.addPage(new DataPage(pager.getAdapter().getCount(), place.getLatLng(), null));
                setPager(pager.getAdapter().getCount() - 1);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getArguments().putInt(KEY_POSITION, position);
    }

    private void setPager(int currentPage) {
        pager_title_strip.setDrawFullUnderline(false);
        PagerAdapter pagerAdapter = new PagerAdapter(getFragmentManager());
        pager.setAdapter(pagerAdapter);
        if (currentPage == 0)
            pager.setCurrentItem(0, true);
        else
            pager.setCurrentItem(pagerAdapter.getCount() - 1, true);
    }


    private void startPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            Log.e(APP, "e = " + e);
        }
    }


}
