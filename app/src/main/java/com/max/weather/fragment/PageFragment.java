package com.max.weather.fragment;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.max.weather.R;
import com.max.weather.event.OnEvent;
import com.max.weather.misk.RealmHelper;
import com.max.weather.model.DataPage;
import com.max.weather.model.FlickrSearchResponse;
import com.max.weather.model.WeatherResult;
import com.max.weather.webapi.CancelableCallback;
import com.max.weather.webapi.FlickrApi;
import com.max.weather.webapi.WeatherApi;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.max.weather.config.Constants.APP;


public class PageFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.weatherIcon) ImageView weatherIcon;
    @Bind(R.id.weatherText) TextView weatherText;
    @Bind(R.id.imageBack) ImageView imageBack;
    @Bind(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private static final String KEY_POSITION = "position";
    private int position;


    public static PageFragment newInstance(int position) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(KEY_POSITION);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void updateLocation(final OnEvent onEvent) {
        Log.d(APP, "get sticky" + onEvent);

        if (onEvent.getPosition() == position) {
            EventBus.getDefault().removeStickyEvent(onEvent);
            updateWeather(new LatLng(onEvent.getLocation().getLatitude(), onEvent.getLocation().getLongitude()));
        }
    }

    private void updateWeather(final LatLng latLng) {
        WeatherApi.getInstance().getWeather(latLng.latitude, latLng.longitude, new CancelableCallback<WeatherResult>(new Callback<WeatherResult>() {

            @Override
            public void success(WeatherResult weatherResult, Response response) {
                swipeRefreshLayout.setRefreshing(false);
                DataPage dataPage;
                dataPage = new DataPage(position, latLng, weatherResult);
                RealmHelper.addPage(dataPage);

                setInterface(dataPage);
            }

            @Override
            public void failure(RetrofitError error) {
                swipeRefreshLayout.setRefreshing(false);
                Log.e(APP, "error = " + error);
            }
        }));

    }

    private void setInterface(DataPage dataPage) {
        if (dataPage != null) {
            WeatherResult weatherResult = dataPage.getWeatherResult();
            if (weatherResult != null) {
                String s = weatherResult.getWeatherIcon();
                Log.d(APP, "s = " + s);
                Picasso.with(getActivity()).load(s).fit().centerInside().into(weatherIcon);
                weatherText.setText(weatherResult.toString());
            } else {
                weatherText.setText(R.string.process_geo_and_weather);
            }

            new PhotoGetter(dataPage.getLatLng()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } else {
            weatherText.setText(R.string.process_geo_and_weather);
        }
    }


    private String getGeoCity(LatLng latLng) {
        Geocoder gcd = new Geocoder(getContext(), Locale.ENGLISH);
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                return addresses.get(0).getLocality();
            } else {
                return null;
            }
        } catch (IOException e) {
            Log.e(APP, "e = " + e);
            return null;
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        DataPage dataPage = RealmHelper.getDataPage(position);
        setInterface(dataPage);
        if (dataPage != null)
            updateWeather(dataPage.getLatLng());
        else
            swipeRefreshLayout.setRefreshing(false);
    }

    private class PhotoGetter extends AsyncTask<Void, Void, String> {

        private LatLng latLng;
        private String cityName;
        private int size;
        private String[] queries = {"street", "city"};


        PhotoGetter(LatLng latLng) {
            super();
            this.latLng = latLng;
        }

        @Override
        protected String doInBackground(Void... params) {

            cityName = getGeoCity(latLng);
            size = 0;
            int i = 0;
            try {
                FlickrSearchResponse response;
                while (size == 0 && i < queries.length - 1) {
                    if (cityName == null)
                        cityName = queries[i];
                    else
                        cityName = cityName + " " + queries[i];
                    response = FlickrApi.getInstance().getImage(latLng, cityName);
                    size = response.getCount();
                    i++;
                    if (response.isSuccess()) {
                        return response.getPhoto();
                    }
                }
                response = FlickrApi.getInstance().getImage(null, queries[queries.length - 1]);
                if (response.isSuccess())
                    return response.getPhoto();
                return null;
            } catch (Exception e) {
                Log.e(APP, "e = " + e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                try {
                    Picasso.with(getContext()).load(s).into(imageBack);
                } catch (Exception e) {
                    Log.e(APP, "e = " + e);
                }
            }
        }
    }

}
