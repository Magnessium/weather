package com.max.weather.model;

import android.content.Context;

import com.max.weather.R;
import com.max.weather.application.WeatherApplication;

import io.realm.RealmObject;

public class Wind extends RealmObject {
    private double speed;
    private double deg;

    @Override
    public String toString() {
        return getCtx().getString(R.string.wind) + " " + speed + getCtx().getString(R.string.m_s) + "\n" + getCtx().getString(R.string.direction) + " " + deg + getCtx().getString(R.string.grad);
    }

    private Context getCtx() {
        return WeatherApplication.getAppContext();
    }

    public Wind() {
        super();
    }
}
