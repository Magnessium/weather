package com.max.weather.model;

import android.content.Context;

import com.max.weather.R;
import com.max.weather.application.WeatherApplication;

import java.util.Locale;

import io.realm.RealmObject;


public class Main extends RealmObject {
    private double temp;
    private int humidity;
    private double pressure;
    private double temp_min;
    private double temp_max;

    @Override
    public String toString() {
        return getCtx().getString(R.string.humidity) + " " + humidity + "%" + "\n" + getCtx().getString(R.string.pressure) + " " + pressure + "\n" + getCtx().getString(R.string.temp) + "\n "
                + getCtx().getString(R.string.now) + " " + getStringTemp(temp) + "\n " + getCtx().getString(R.string.today) + " " + getStringTemp(temp_min) + " .. " + getStringTemp(temp_max);
    }

    private String getStringTemp(double temp) {
        if (temp > 0) {
            return "+" + getFormatted(temp);
        } else if (temp < 0) {
            return "-" + getFormatted(temp);
        } else {
            return getFormatted(temp);
        }
    }

    private String getFormatted(double temp) {
        return String.format(Locale.getDefault(), "%,.1f", temp);
    }

    private Context getCtx() {
        return WeatherApplication.getAppContext();
    }

    public Main() {
        super();
    }
}
