package com.max.weather.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.max.weather.R;
import com.max.weather.application.WeatherApplication;

import io.realm.RealmObject;

public class Rain extends RealmObject {
    @SerializedName("3h") private int value;

    @Override
    public String toString() {
        return getCtx().getString(R.string.water_mm) + " " + value + getCtx().getString(R.string.mm);
    }

    public Rain() {
        super();
    }

    private Context getCtx() {
        return WeatherApplication.getAppContext();
    }
}
