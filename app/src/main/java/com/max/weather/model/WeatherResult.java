package com.max.weather.model;

import io.realm.RealmList;
import io.realm.RealmObject;

public class WeatherResult extends RealmObject {

    private Sys sys;
    private RealmList<WeatherItem> weather;
    private Main main;
    private Wind wind;
    private Rain rain;
    private Clouds clouds;

    @Override
    public String toString() {
        return sys + "\n" + weather.get(0) + "\n" + main + "\n" + wind + "\n" + (rain != null ? rain : "\n") + "\n" + clouds;
    }

    public String getWeatherIcon() {
        if (weather == null) return null;
        if (weather.size() > 0) return weather.get(0).getIcon();
        return null;
    }

}
