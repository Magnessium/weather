package com.max.weather.model;

import com.max.weather.R;
import com.max.weather.application.WeatherApplication;

import io.realm.RealmObject;


public class Clouds extends RealmObject {
    private int all;

    @Override
    public String toString() {
        return WeatherApplication.getAppContext().getString(R.string.clouds) + " " + all + "%";
    }

    public Clouds() {
        super();
    }
}
