package com.max.weather.model;

import io.realm.RealmObject;

import static com.max.weather.config.Constants.EXT_ICON;
import static com.max.weather.config.Constants.WEATHER_BASE_POINT_ICON;


public class WeatherItem extends RealmObject {
    private int id;
    private String main;
    private String description;
    private String icon;

    @Override
    public String toString() {
        return main + ", " + description;
    }

    public WeatherItem() {
        super();
    }

    String getIcon() {
        if (icon != null && icon.length() > 0)
            return WEATHER_BASE_POINT_ICON + icon + EXT_ICON;
        else
            return null;
    }
}
