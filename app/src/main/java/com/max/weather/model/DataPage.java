package com.max.weather.model;

import com.google.android.gms.maps.model.LatLng;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class DataPage extends RealmObject {

    @PrimaryKey
    private int page;

    private double lat;
    private double lon;
    private WeatherResult weatherResult;

    public DataPage() {
        super();
    }

    public DataPage(int page, LatLng latLng, WeatherResult weatherResult) {
        this.page = page;
        this.lat = latLng.latitude;
        this.lon = latLng.longitude;
        this.weatherResult = weatherResult;
    }

    public int getPage() {
        return page;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lon);
    }

    public WeatherResult getWeatherResult() {
        return weatherResult;
    }
}
