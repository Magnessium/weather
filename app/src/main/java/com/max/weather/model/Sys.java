package com.max.weather.model;

import com.max.weather.R;
import com.max.weather.application.WeatherApplication;
import com.max.weather.misk.DateHelper;

import io.realm.RealmObject;

import static com.max.weather.config.Constants.SUN_TIME_FORMAT;

public class Sys extends RealmObject {
    private String country;
    private long sunrise;
    private long sunset;

    public Sys() {
        super();
    }

    private String getSunriseText() {
        return String.format("%s %s", WeatherApplication.getAppContext().getString(R.string.sunrize), DateHelper.getDateStringFromMs(sunrise, SUN_TIME_FORMAT));
    }

    private String getSunsetText() {
        return String.format("%s %s", WeatherApplication.getAppContext().getString(R.string.sunset), DateHelper.getDateStringFromMs(sunset, SUN_TIME_FORMAT));
    }

    private String getCountry() {
        return String.format("%s %s", WeatherApplication.getAppContext().getString(R.string.country), country != null ? country : WeatherApplication.getAppContext().getString(R.string.not_define));
    }

    @Override
    public String toString() {
        return String.format("%s\n%s\n%s", getCountry(), getSunriseText(), getSunsetText());
    }
}
