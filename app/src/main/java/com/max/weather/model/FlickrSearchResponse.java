package com.max.weather.model;

import java.util.ArrayList;


public class FlickrSearchResponse {
    private Photos photos;

    private class Photos {
        ArrayList<FlickrImage> photo;
    }

    public boolean isSuccess() {
        return photos != null && photos.photo != null && photos.photo.size() > 0;
    }

    public String getPhoto() {
        if (!isSuccess()) return null;
        return photos.photo.get(0).getUrl1();
    }

    public int getCount() {
        if (!isSuccess()) return 0;
        return photos.photo.size();
    }
}
