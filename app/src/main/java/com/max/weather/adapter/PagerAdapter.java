package com.max.weather.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.max.weather.R;
import com.max.weather.application.WeatherApplication;
import com.max.weather.fragment.PageFragment;
import com.max.weather.misk.RealmHelper;
import com.max.weather.model.DataPage;

import java.util.Locale;

import io.realm.RealmResults;


public class PagerAdapter extends FragmentPagerAdapter {

    private int count;
    private RealmResults<DataPage> realmResults;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        init();
    }

    private void init() {
        realmResults = RealmHelper.getDataPages();
        if (realmResults != null && realmResults.size() > 0) {
            count = realmResults.size();
        } else {
            count = 1;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return getTitle(position);
    }


    @Override
    public int getCount() {
        return count;
    }


    private CharSequence getTitle(int position) {
        if (position == 0 || realmResults == null)
            return WeatherApplication.getAppContext().getString(R.string.your_location);

        for (DataPage page :
                realmResults) {
            if (page.getPage() == position)
                return String.format(Locale.ENGLISH, "Lat: %,.2f, Lon: %,.2f", realmResults.get(position).getLatLng().latitude, realmResults.get(position).getLatLng().longitude);
        }

        return WeatherApplication.getAppContext().getString(R.string.your_location);
    }

}
