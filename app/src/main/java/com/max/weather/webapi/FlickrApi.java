package com.max.weather.webapi;


import android.annotation.SuppressLint;

import com.google.android.gms.maps.model.LatLng;
import com.max.weather.config.Constants;
import com.max.weather.model.FlickrSearchResponse;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import static com.max.weather.config.Constants.FLICKR_API_ACCURACY;
import static com.max.weather.config.Constants.FLICKR_API_CONTENT_TYPE;
import static com.max.weather.config.Constants.FLICKR_API_FORMAT;
import static com.max.weather.config.Constants.FLICKR_API_KEY;
import static com.max.weather.config.Constants.FLICKR_API_NOJSON;
import static com.max.weather.config.Constants.FLICKR_API_PAGE;
import static com.max.weather.config.Constants.FLICKR_API_PER_PAGE;
import static com.max.weather.config.Constants.FLICKR_API_PRIVACY_FILTER;
import static com.max.weather.config.Constants.FLICKR_API_SEARCH;


public class FlickrApi {

    private static final FlickrApi instance = new FlickrApi();

    private FlickrApi() {
        initConnection();
    }

    public static FlickrApi getInstance() {
        return instance;
    }

    private FlickrInterface service;

    private void initConnection() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.FLICKR_BASE_POINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(getClient()))
                .build();

        service = restAdapter.create(FlickrInterface.class);
    }

    private OkHttpClient getClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @SuppressLint("TrustAllX509TrustManager")
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @SuppressLint("TrustAllX509TrustManager")
                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setFollowSslRedirects(true);
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier")
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });


            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public FlickrSearchResponse getImage(LatLng latLng, String text) {
        return service.searchImage(FLICKR_API_SEARCH, FLICKR_API_KEY, FLICKR_API_ACCURACY,
                latLng != null ? latLng.latitude : null, latLng != null ? latLng.longitude : null,
                FLICKR_API_FORMAT, FLICKR_API_NOJSON, FLICKR_API_CONTENT_TYPE, FLICKR_API_PAGE, FLICKR_API_PER_PAGE, FLICKR_API_PRIVACY_FILTER,
                text);
    }

}
