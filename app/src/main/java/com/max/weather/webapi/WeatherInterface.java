package com.max.weather.webapi;


import com.max.weather.model.WeatherResult;

import retrofit.http.GET;
import retrofit.http.Query;

interface WeatherInterface {

    @GET("/data/2.5/weather")
    void weather(@Query("lat") double lat, @Query("lon") double lon, @Query("units") String units,
                 @Query("APPID") String token,
                 @Query("lang") String lang,
                 CancelableCallback<WeatherResult> callback);

}
