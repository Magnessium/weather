package com.max.weather.webapi;


import com.max.weather.model.FlickrSearchResponse;

import retrofit.http.GET;
import retrofit.http.Query;

interface FlickrInterface {

    @GET("/rest")
    FlickrSearchResponse searchImage(@Query("method") String method, @Query("api_key") String apiKey,
                                     @Query("accuracy") int accuracy,
                                     @Query("lat") Double lat,
                                     @Query("lon") Double lon,
                                     @Query("format") String format,
                                     @Query("nojsoncallback") String nojson,
                                     @Query("content_type") int contentType,
                                     @Query("page") int page,
                                     @Query("per_page") int perPage,
                                     @Query("privacy_filter") int privacyFilter,
                                     @Query("text") String text);
}
