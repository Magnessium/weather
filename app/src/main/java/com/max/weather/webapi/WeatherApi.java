package com.max.weather.webapi;


import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.max.weather.application.WeatherApplication;
import com.max.weather.config.Constants;
import com.max.weather.model.WeatherResult;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

import static com.max.weather.config.Constants.WEATHER_APP_ID;
import static com.max.weather.config.Constants.WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS;
import static com.max.weather.config.Constants.WEATHER_SIZE_OF_CACHE;
import static com.max.weather.config.Constants.WEATHER_UNITS;


public class WeatherApi {

    private static final WeatherApi instance = new WeatherApi();

    private WeatherApi() {
        initConnection();
    }

    public static WeatherApi getInstance() {
        return instance;
    }

    private WeatherInterface service;

    private void initConnection() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.WEATHER_BASE_POINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(getClient()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        if (isOnline()) {
                            request.addHeader("Cache-Control", "public, max-age=" + WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS);
                        } else {
                            request.addHeader("Cache-Control", "public, only-if-cached, max-stale=" + WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS);
                        }
                    }
                })
                .build();

        service = restAdapter.create(WeatherInterface.class);
    }

    private OkHttpClient getClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @SuppressLint("TrustAllX509TrustManager")
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @SuppressLint("TrustAllX509TrustManager")
                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setFollowSslRedirects(true);
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier")
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            Cache cache = new Cache(new File(WeatherApplication.getAppContext().getCacheDir(), "http"), WEATHER_SIZE_OF_CACHE);
            okHttpClient.setCache(cache);
            okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

            // Add Cache-Control Interceptor
            okHttpClient.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    Log.d("OkHttp REQUEST", request.toString());
                    Log.d("OkHttp REQUEST Headers", request.headers().toString());
                    Response response = chain.proceed(request);
                    if (request.method().equals("GET")) {

                        response = response.newBuilder()
                                .header("Cache-Control", String.format("public, max-age=%d, max-stale=%d", WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS, WEATHER_RESPONSE_CACHE_LIFESPAN_IN_SECONDS))
                                .build();

                        Log.d("OkHttp RESPONSE", response.toString());
                        Log.d("OkHttp RESPONSE Headers", response.headers().toString());
                    }
                    return response;


                }
            });


            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) WeatherApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void getWeather(double lat, double lon, CancelableCallback<WeatherResult> callback) {
        service.weather(lat, lon, WEATHER_UNITS, WEATHER_APP_ID, Locale.getDefault().getLanguage(), callback);
    }

}
