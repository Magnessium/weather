package com.max.weather.webapi;


import android.util.Log;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CancelableCallback<T> implements Callback<T> {

    private final Callback callback;

    public CancelableCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    public void success(T o, Response response) {
        try {
            callback.success(o, response);
        } catch (Throwable ignore) {
            Log.e("RETROFIT", "NullPointerException in success");
        }
    }

    @Override
    public void failure(RetrofitError error) {
        try {
            callback.failure(error);
        } catch (Throwable ignore) {
            Log.e("RETROFIT", "NullPointerException in failure");
        }
    }
}
