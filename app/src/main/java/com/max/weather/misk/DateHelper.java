package com.max.weather.misk;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DateHelper {


    public static String getDateStringFromMs(long ms, String s_format) {
        String s = "";

        SimpleDateFormat formatter = new SimpleDateFormat(s_format, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ms);

        return formatter.format(calendar.getTime());
    }

}
