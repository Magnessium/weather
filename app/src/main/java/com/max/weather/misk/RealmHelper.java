package com.max.weather.misk;

import com.max.weather.model.DataPage;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmHelper {

    public static void addPage(final DataPage dataPage) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        DataPage realmDataPage = realm.copyToRealmOrUpdate(dataPage);
        realm.commitTransaction();
    }

    public static DataPage getDataPage(int page) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DataPage.class).equalTo("page", page).findFirst();
    }

    public static RealmResults<DataPage> getDataPages() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DataPage.class).findAll();
    }


}
